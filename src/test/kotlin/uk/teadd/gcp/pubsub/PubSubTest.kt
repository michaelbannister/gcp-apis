package uk.teadd.gcp.pubsub

import com.google.api.core.ApiFutures
import com.google.cloud.ServiceOptions
import com.google.cloud.pubsub.v1.Publisher
import com.google.cloud.pubsub.v1.TopicAdminClient
import com.google.pubsub.v1.PubsubMessage
import com.google.pubsub.v1.Topic
import io.mockk.*
import org.awaitility.kotlin.atMost
import org.awaitility.kotlin.await
import org.awaitility.kotlin.until
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.time.Duration

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PubSubIntegrationTest {

    val projectId = ServiceOptions.getDefaultProjectId()!!
    val topicName = "projects/${projectId}/topics/pubsubtest"
    lateinit var topic: Topic

    @BeforeAll
    fun `set up pubsub topic`() {
        TopicAdminClient.create().use { client ->
            topic = client.createTopic(topicName)
        }
    }

    @AfterAll
    fun `destroy pubsub topic`() {
        TopicAdminClient.create().use { client ->
            client.deleteTopic(topicName)
        }
    }

    @Test
    fun `test pubsub integrated`() {
        val publisher = PubsubMessagePublisher(Publisher.newBuilder(topic.name).build())
        val updateEventHandler = UpdateEventHandler(publisher)
        updateEventHandler.handleUpdateEvent("foo", "bar")

        await atMost Duration.ofSeconds(5) until { updateEventHandler.successCount == 1 }
    }

}

class UpdateEventHandlerTest {
    @Test
    fun `test UpdateEventHandler success`() {
        val publisher = mockk<PubsubMessagePublisher>()
        every { publisher.publishMessage(any(), captureLambda(), any()) } answers { lambda<(String?) -> Unit>().invoke("ok") }
        val updateEventHandler = UpdateEventHandler(publisher)

        updateEventHandler.handleUpdateEvent("foo", "bar")

        expectThat(updateEventHandler).get(UpdateEventHandler::successCount).isEqualTo(1)
    }

    @Test
    fun `test UpdateEventHandler failure`() {
        val publisher = mockk<PubsubMessagePublisher>()
        every { publisher.publishMessage(any(), any(), captureLambda()) } answers { lambda<(Throwable?) -> Unit>().invoke(Exception()) }
        val updateEventHandler = UpdateEventHandler(publisher)

        updateEventHandler.handleUpdateEvent("foo", "bar")

        expectThat(updateEventHandler).get(UpdateEventHandler::failureCount).isEqualTo(1)
    }
}

class PubsubMessagePublisherTest {

    val successCallback = spyk({ _: String? -> }, name = "successCallback")
    val failureCallback = spyk({ _: Throwable? -> }, "failureCallback")

    @Test
    fun `should trigger success callback on ApiFuture completion`() {
        val gcpPublisher = mockk<Publisher>()
        every { gcpPublisher.publish(any()) } returns ApiFutures.immediateFuture("ok")

        val messagePublisher = PubsubMessagePublisher(gcpPublisher)
        messagePublisher.publishMessage(PubsubMessage.newBuilder().build(),
                successCallback,
                failureCallback)

        verify(exactly = 1) { successCallback("ok") }
        verify(exactly = 0) { failureCallback(any()) }
    }

    @Test
    fun `should trigger failure callback on ApiFuture failure`() {
        val gcpPublisher = mockk<Publisher>()
        val pubsubException = Exception("argh!")
        every { gcpPublisher.publish(any()) } returns ApiFutures.immediateFailedFuture(pubsubException)

        val updateEventMessagePublisher = PubsubMessagePublisher(gcpPublisher)
        updateEventMessagePublisher.publishMessage(PubsubMessage.newBuilder().build(),
                successCallback,
                failureCallback)

        verify(exactly = 0) { successCallback(any()) }
        verify(exactly = 1) { failureCallback(pubsubException) }

    }
}
