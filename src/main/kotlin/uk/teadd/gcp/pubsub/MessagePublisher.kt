package uk.teadd.gcp.pubsub

import com.google.api.core.ApiFutureCallback
import com.google.api.core.ApiFutures
import com.google.cloud.pubsub.v1.Publisher
import com.google.common.util.concurrent.MoreExecutors
import com.google.pubsub.v1.PubsubMessage

interface MessagePublisher {
    fun publishMessage(message: PubsubMessage, onSuccess: (String?) -> Unit, onFailure: (Throwable?) -> Unit)
}

class PubsubMessagePublisher(private val publisher: Publisher) : MessagePublisher {

    override fun publishMessage(message: PubsubMessage, onSuccess: (String?) -> Unit, onFailure: (Throwable?) -> Unit) {
        val apiFuture = publisher.publish(message)

        ApiFutures.addCallback(apiFuture, object : ApiFutureCallback<String> {
            override fun onSuccess(result: String?) {
                onSuccess(result)
            }

            override fun onFailure(t: Throwable?) {
                onFailure(t)
            }
        }, MoreExecutors.directExecutor())
    }
}

