package uk.teadd.gcp.pubsub

import com.google.protobuf.ByteString
import com.google.pubsub.v1.PubsubMessage
import java.util.concurrent.atomic.AtomicInteger

class UpdateEventHandler(private val publisher: MessagePublisher) {

    private val successes = AtomicInteger(0)
    private val failures = AtomicInteger(0)

    fun handleUpdateEvent(eventId: String, eventType: String) {
        val message = UpdateEvent.createUpdateEventMessage(eventId, eventType)

        publisher.publishMessage(message,
                onSuccess = { successes.incrementAndGet() },
                onFailure = { failures.incrementAndGet() })
    }

    val successCount get() = successes.get()
    val failureCount get() = failures.get()
}

object UpdateEvent {
    fun createUpdateEventMessage(eventId: String, eventType: String): PubsubMessage =
            PubsubMessage.newBuilder()
                    .setData(ByteString.copyFromUtf8("""{"eventId":"$eventId","eventType":"$eventType"}"""))
                    .build()
}
