import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.70"
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation(platform("com.google.cloud:google-cloud-bom:0.122.3-alpha"))
    implementation("com.google.cloud:google-cloud-core")
    implementation("com.google.cloud:google-cloud-pubsub")
    
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")

    testImplementation("io.strikt:strikt-core:0.24.0")
    testImplementation("org.awaitility:awaitility-kotlin:4.0.2")
    testImplementation("io.mockk:mockk:1.9.3")
}

tasks.withType(Test::class.java).configureEach {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.withType(KotlinCompile::class.java).configureEach {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClassName = "org.bannisters.AppKt"
}
